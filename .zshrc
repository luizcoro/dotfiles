source "$HOME/.cache/p10k-instant-prompt-luiz.zsh"

HISTFILE=~/.config/zsh/history
HISTSIZE=1000
SAVEHIST=1000
CASE_SENSITIVE="false"

setopt auto_pushd
setopt pushd_ignore_dups
setopt pushd_minus
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt inc_append_history
setopt share_history
setopt complete_aliases

eval `dircolors ~/.dir_colors`

autoload -Uz compinit; compinit
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.config/zsh/cache/completion
zstyle ':completion:*' menu select
zstyle ':completion:*' file-sort time
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*:approximate:*' max-errors 1 numeric
zstyle ':completion:*:functions' ignored-patterns '_*'

# autoload -Uz bracketed-paste-magic; zle -N bracketed-paste bracketed-paste-magic
# autoload -Uz url-quote-magic; zle -N self-insert url-quote-magic
# zstyle ':bracketed-paste-magic' active-widgets '.self-*'


source ~/.config/zsh/plugins.zsh
source ~/.config/zsh/alias.zsh
source ~/.config/zsh/mappings.zsh
source ~/.config/zsh/themes/p10k.zsh


