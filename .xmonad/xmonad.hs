import qualified Data.Map as M
import Data.Monoid
import Data.Semigroup
import Graphics.X11.ExtraTypes.XF86
import System.Exit
import XMonad
import XMonad.Actions.SpawnOn
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicProperty
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import qualified XMonad.Layout.Dwindle as Dwindle
import XMonad.Layout.Gaps
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import qualified XMonad.StackSet as W
import XMonad.Util.Cursor
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.Scratchpad
import XMonad.Util.SpawnOnce

myTextEditor    = "nvim"
myTerminal      = "alacritty"

myFocusFollowsMouse = True
myClickJustFocuses  = False

myBorderWidth        = 1
myModMask            = mod4Mask
myWorkspaces         = ["1","2","3","4","5","6","7","8","9","0"]
myNormalBorderColor  = "#2E3440"
myFocusedBorderColor = "#BF616A"

scratchpads =
  [ NS
      "writingScratch"
      "alacritty --class WritingScratch -e nvim"
      (resource =? "WritingScratch")
      (customFloating mediumRect),
    NS
      "drawingScratch"
      "xournalpp --class DrawingScratch"
      (className =? "DrawingScratch")
      (customFloating bigTopLeftSkewedRect),
    NS
      "rScratch"
      "alacritty --class RScratch -e R"
      (resource =? "RScratch")
      (customFloating mediumRect),
    NS
      "terminalScratch"
      "alacritty --class TerminalScratch"
      (resource =? "TerminalScratch")
      (customFloating mediumRect)
  ]
  where
    bigRect = W.RationalRect (1 / 14) (1 / 14) (6 / 7) (6 / 7)
    bigTopLeftSkewedRect = W.RationalRect (1 / 28) (1 / 28) (6 / 7) (6 / 7)
    mediumRect = W.RationalRect (1 / 6) (1 / 6) (2 / 3) (2 / 3)
    smallRect = W.RationalRect (1 / 3) (1 / 4) (1 / 2) (1 / 2)

toggleFloat w =
  windows
    ( \s ->
        if M.member w (W.floating s)
          then W.sink w s
          else (W.float w (W.RationalRect (1 / 6) (1 / 6) (2 / 3) (2 / 3)) s)
    )


plusOne d i | d `elem` [U, D, L, R] = i + 1
            | otherwise       = i
minusOne d i | d `elem` [U, D, L, R] = i - 1
             | otherwise       = i

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm,               xK_p     ), spawn "rofi -show drun -theme my-nord")
    , ((modm .|. shiftMask , xK_BackSpace     ), spawn "qutebrowser")
    , ((modm .|. controlMask .|. shiftMask, xK_w),
        namedScratchpadAction scratchpads "writingScratch")
    , ((modm .|. controlMask .|. shiftMask, xK_d),
        namedScratchpadAction scratchpads "drawingScratch")
    , ((modm .|. controlMask .|. shiftMask, xK_r),
        namedScratchpadAction scratchpads "rScratch")
    , ((modm .|. controlMask .|. shiftMask, xK_t),
        namedScratchpadAction scratchpads "terminalScratch")
    , ((modm .|. shiftMask, xK_c     ), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm              , xK_f), sendMessage $ Toggle FULL)
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modm,               xK_n     ), refresh)
    , ((modm,               xK_Tab   ), windows W.focusDown)
    , ((modm .|. controlMask, xK_Tab), windows $ W.swapMaster . W.focusDown)
    , ((modm,               xK_j     ), windows W.focusDown)
    , ((modm .|. controlMask, xK_j), windows $ W.swapMaster . W.focusDown)
    , ((modm,               xK_k     ), windows W.focusUp  )
    , ((modm,               xK_m     ), windows W.focusMaster  )
    , ((modm,               xK_Return), windows W.shiftMaster)
    , ((modm .|. controlMask, xK_Return), windows W.swapMaster)
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
    , ((modm,               xK_h     ), sendMessage Shrink)
    , ((modm,               xK_l     ), sendMessage Expand)
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))
    , ((modm              , xK_b     ), sendMessage ToggleStruts)
    , ((modm              , xK_a     ), withFocused toggleFloat)
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")
    , ((0, xF86XK_AudioMute     ),    spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ((0, xF86XK_AudioLowerVolume),  spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
    , ((0, xF86XK_AudioRaiseVolume),  spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
    , ((0, xF86XK_AudioPrev     ),    spawn "playerctl previous")
    , ((0, xF86XK_AudioPlay     ),    spawn "playerctl play-pause")
    , ((0, xF86XK_AudioNext     ),    spawn "playerctl next")
    , ((0, xF86XK_MonBrightnessDown), spawn "xbacklight -dec 5")
    , ((0, xF86XK_MonBrightnessUp),   spawn "xbacklight -inc 5")
    , ((0, xK_Print), spawn "maim ~/Pictures/$(date +%Y-%m-%d-%H:%M.%S).png")
    , ((shiftMask, xK_Print), spawn "maim -s ~/Pictures/$(date +%Y-%m-%d-%H:%M.%S).png")
    , ((controlMask, xK_Print), spawn "maim -i $(xdotool getactivewindow) ~/Pictures/$(date +%Y-%m-%d-%H:%M.%S).png")
    , ((modm .|. controlMask, xK_g), sendMessage $ ToggleGaps)
    , ((modm .|. controlMask, xK_w), sendMessage $ weakModifyGaps plusOne)
    , ((modm .|. controlMask, xK_q), sendMessage $ weakModifyGaps minusOne)
    , ((modm .|. controlMask, xK_s), sendMessage $ setGaps [(U,5), (D,5), (R,5), (L,5)])
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [
            xK_parenleft,
            xK_parenright,
            xK_braceright,
            xK_plus,
            xK_braceleft,
            xK_bracketright,
            xK_bracketleft,
            xK_exclam,
            xK_equal,
            xK_asterisk
        ]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

myLayout = smartBorders
    $ mkToggle (NOBORDERS ?? FULL ?? EOT)
    $ avoidStruts
    $ gaps [(U,5), (D,5), (R,5), (L,5)]
    $ spacing 5 $ (fib ||| tiled ||| Full)
  where
     tiled   = Tall nmaster delta ratio
     fib = Dwindle.Dwindle R Dwindle.CW goldenNumberInv 1.1
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100
     goldenNumber = 6/7
     goldenNumberInv = 2-goldenNumber

-- doLower :: ManageHook
-- doLower = ask >>= \w -> liftX $ withDisplay $ \dpy -> io (lowerWindow dpy w) >> mempty

myManageHook = composeAll
    [ isFullscreen              --> doFullFloat
    , isDialog              --> doFloat
    , className =? "mpv"        --> doFloat
    , className =? "Peek"           --> doFloat
    , className =? "Gimp"           --> doFloat
    , className =? "Float"           --> doFloat
    , className =? "FloatCenter"           --> doCenterFloat
    , className =? "polybar" --> doLower
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

myEventHook = mempty
myLogHook = return ()

myStartupHook =  do
    setDefaultCursor xC_left_ptr
    spawnOnce "nitrogen --restore &"
    spawnOnce "picom --config /home/luiz/.config/picom/picom.conf &"
    -- spawnOnOnce "0" "neomutt-gui"
    -- spawnOnOnce "8" "easyeffects"

-- ewmhFullscreen 
main = do
    xmproc <- spawnPipe "/home/luiz/.config/polybar/launch.sh"
    xmonad $ docks $ ewmhFullscreen . ewmh $ def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        keys               = myKeys,
        mouseBindings      = myMouseBindings,
        layoutHook         = myLayout,
        manageHook         = manageDocks
            <+> manageSpawn
            <+> myManageHook
            <+> namedScratchpadManageHook scratchpads,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch rofi",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
