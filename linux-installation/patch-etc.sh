#!/bin/bash

cp etc/hostname /etc/hostname
cp etc/locale.conf /etc/locale.conf
cp etc/vconsole.conf /etc/vconsole.conf
cp etc/pacman/pacman.conf /etc/pacman/pacman.conf
cp etc/pacman/pacman.d/mirrorlist /etc/pacman/pacman.d/mirrorlist
cp etc/X11/xorg.conf.d/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf

