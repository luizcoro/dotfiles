export EDITOR=nvim
export VISUAL=nvim
# export VISUAL=neovide
export TERMINAL=alacritty
export BROWSER=qutebrowser
export ANDROID_SDK_ROOT=/opt/android-sdk/
export CHROME_EXECUTABLE=/usr/bin/google-chrome-stable
export JAVA_HOME=/usr/lib/jvm/default
export PATH=$JAVA_HOME/bin:/usr/bin/vendor_perl:$PATH


export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS
      --color fg:#D8DEE9,bg:#2E3440,hl:#A3BE8C
      --color fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C
      --color pointer:#BF616A,info:#4C566A,spinner:#4C566A
      --color header:#4C566A,prompt:#81A1C1,marker:#EBCB8B"

export ZSH_CACHE_DIR="$HOME/.config/zsh/cache"

export MANPAGER="nvim -u /home/luiz/.config/nvim/init-man.lua +Man!"

# export NeovideMultiGrid=1

export STXXLCFG=/home/luiz/.config/stxxl/config
export OPENAI_API_KEY=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
