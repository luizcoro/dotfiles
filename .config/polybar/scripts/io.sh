#!/bin/bash

while read line
do
    bytes=$(sed -E "s/:|B//g; s/k/000/g; s/m/000000/g" <<< $line)
    numfmt --to=si $bytes | xargs printf "sda:%5s /%5s    sdb:%5s /%5s\n"
done < <(dstat --noupdate -dD sda,sdb 3)

# while read line
# do
#     # IFS=':' read -a parsed <<< "$line"
#     printf "total:%5s /%5s\n" ${line[@]}
# done < <(dstat --noupdate -dD total 3)
