source ~/.config/zsh/plugins/completion.zsh
source ~/.config/zsh/plugins/key-bindings.zsh
source ~/.config/zsh/plugins/last-working-dir.plugin.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme


