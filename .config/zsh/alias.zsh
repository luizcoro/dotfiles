alias make='make --no-print-directory'
alias l='exa --icons -gl'
alias ls='exa --icons'
alias tree='exa --icons -T'
alias cat='bat'
# alias rm='safe-rm'
alias ctags='ctags --options="/home/luiz/.config/ctags/config" '
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias nvimc='nvim ~/.config/nvim/init.lua'
alias nvim='nvim --listen /tmp/nvimsocket'
alias ua-drop-caches='sudo paccache -rk3; yay -Sc --aur --noconfirm'
alias ua-update-all='export TMPFILE="$(mktemp)"; \
    sudo true; \
    curl -s "https://archlinux.org/mirrorlist/?country=BR&use_mirror_status=on" \
        | sed -e 's/^#Server/Server/' -e '/^#/d' \
        | rankmirrors -n 6 - > $TMPFILE \
      && sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist-backup \
      && sudo mv $TMPFILE /etc/pacman.d/mirrorlist \
      && ua-drop-caches \
      && yay -Syyu --noconfirm neovim-git hyprland-git xdg-desktop-portal-hyprland-git'
      # && yay -Syyu --noconfirm neovim-git hyprland-git waybar-hyprland-git xdg-desktop-portal-hyprland-git mesa-git lib32-mesa-git'
