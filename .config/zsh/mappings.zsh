bindkey -v
export KEYTIMEOUT=1
bindkey '^w' backward-kill-word
bindkey '^h' backward-delete-char
bindkey '^?' backward-delete-char
bindkey '^p' reverse-menu-complete
bindkey '^n' expand-or-complete
bindkey '^k' up-history
bindkey '^j' down-history
bindkey '\x1b[13;5u' autosuggest-execute
bindkey '\x1b[13;2u' autosuggest-accept
