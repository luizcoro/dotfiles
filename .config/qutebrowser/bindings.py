config.bind('<Ctrl-o>', 'back')
config.bind('<Ctrl-i>', 'forward')
config.bind(' l', 'spawn --userscript qute-pass -d "fuzzel --prompt \'Open store: \' -w 52 -d"')
config.bind(' ul', 'spawn --userscript qute-pass --username-only -d "fuzzel --prompt \'Open store: \' -w 52 -d"')
config.bind(' pl', 'spawn --userscript qute-pass --password-only -d "fuzzel --prompt \'Open store: \' -w 52 -d"')
config.bind(' ol', 'spawn --userscript qute-pass --otp-only -d "fuzzel --prompt \'Open store: \' -w 52 -d"')
config.bind(' d', 'spawn ~/.config/qutebrowser/open_download')
config.bind(' r', 'spawn --userscript readability')
config.bind(' m', 'spawn --userscript view_in_mpv --fs {url}')
config.bind(' M', 'hint links spawn --userscript view_in_mpv --fs {hint-url}')
config.bind(';M', 'hint --rapid links spawn --userscript view_in_mpv --fs {hint-url}')
config.bind(',ps', 'hint images spawn qutebrowser https://yandex.com/images/search?rpt=imageview&url={hint-url}')


