config.load_autoconfig(False)
# c.qt.args = ["enable-native-gpu-memory-buffers", "enable-gpu-rasterization", "enable-accelerated-video-decode", "use-gl=egl", "ignore-gpu-blacklist"]
# c.qt.args = [ "enable-webrtc-pipewire-capturer" ]
# c.qt.args = [ "enable-features=WebRTCPipeWireCapture" ]


config.set("content.cookies.accept", "all", "chrome-devtools://*")
config.set("content.cookies.accept", "all", "devtools://*")
config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}",
    "https://web.whatsapp.com/",
)
config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}",
    "https://accounts.google.com/*",
)
config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36",
    "https://*.slack.com/*",
)

config.set("content.images", True, "chrome-devtools://*")
config.set("content.images", True, "devtools://*")
config.set("content.javascript.enabled", True, "chrome-devtools://*")
config.set("content.javascript.enabled", True, "devtools://*")
config.set("content.javascript.enabled", True, "chrome://*/*")
config.set("content.javascript.enabled", True, "qute://*/*")

config.set(
    "content.register_protocol_handler",
    True,
    "https://mail.google.com?extsrc=mailto&url=%25s",
)

c.url.searchengines = {
    "DEFAULT": "https://duckduckgo.com/?q={}",
    "g": "https://www.google.com.br/search?q={}",
    "gi": "https://www.google.com.br/search?q={}&tbm=isch",
    "gm": "https://www.google.com.br/maps/place/{}",
    "gs": "https://www.semanticscholar.org/search?q={}&sort=relevance",
    "gt": "https://translate.google.com.br/#auto/en/{}",
    "d": "https://duckduckgo.com/html/?q={}",
    "w": "https://en.wikipedia.org/wiki/{}",
    "aw": "https://wiki.archlinux.org/?search={}",
    "dic": "http://www.merriam-webster.com/dictionary/{}",
    "dicbr": "https://www.priberam.pt/DLPO/{}",
    "thes": "http://www.merriam-webster.com/thesaurus/{}",
    "thesbr": "http://www.sinonimos.com.br/{}",
    "imdb": "http://www.imdb.com/find?q={}",
    "rotten": "http://www.rottentomatoes.com/search/?search={}",
    "git": "https://github.com/search?q={}",
    "stack": "http://stackoverflow.com/search?q={}",
    "urban": "http://www.urbandictionary.com/define.php?term={}",
    "yt": "https://youtube.com/results/?search_query={}",
    "pirate": "https://thepiratebay.org/search/{}",
    "nyaa": "https://nyaa.si/?f=0&c=0_0&q={}",
    "al": "https://myanimelist.net/search/all?q={}&cat=all",
}


c.fonts.default_size = "14pt"
c.fonts.tabs.selected = "bold 11pt"
c.fonts.tabs.unselected = "11pt"
c.fonts.downloads = "12pt"

c.zoom.default = "135%"
c.content.blocking.method = "both"

config.source("nord-qutebrowser.py")
config.source("bindings.py")
