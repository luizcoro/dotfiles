require("config.options")
require("config.autocmds").on_startup()
require("config.bootstrap-lazy")

vim.api.nvim_create_autocmd("User", {
	pattern = "VeryLazy",
	callback = function()
		require("config.keymaps").on_verylazy()
		require("config.autocmds").on_verylazy()
	end,
})
