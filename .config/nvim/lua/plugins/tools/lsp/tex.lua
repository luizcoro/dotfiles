local M = {}

M.ltex_config = {
    cmd = { "ltex-ls" },
    filetypes = { "tex", "plaintex", "bib", "markdown" },
    root_dir = function(filename)
        local lsp = require("lspconfig")
        return lsp.util.root_pattern(".git", "manuscript.tex", "Makefile", "template.tex")(filename)
            or lsp.util.path.dirname(filename)
    end,
    settings = {
        ltex = {
            enabled = { "tex", "latex", "bibtex", "markdown" },
            language = "en-US",
            additionalRules = {
                enablePickyRules = true,
                motherTongue = "pt-BR",
            },
        },
    },
}

return M
