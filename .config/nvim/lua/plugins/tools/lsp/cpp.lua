local M = {}

local function switch_source_header(bufnr, mode)
    local lsp = require("lspconfig")
    bufnr = lsp.util.validate_bufnr(bufnr)
    local clangd_client = lsp.util.get_active_client_by_name(bufnr, "clangd")
    local params = { uri = vim.uri_from_bufnr(bufnr) }
    if clangd_client then
        clangd_client.request("textDocument/switchSourceHeader", params, function(err, result)
            if err then
                error(tostring(err))
            end
            if not result then
                print("Corresponding file cannot be determined")
                return
            end
            vim.api.nvim_command(mode .. " " .. vim.uri_to_fname(result))
        end, bufnr)
    else
        print("method textDocument/switchSourceHeader is not supported by any servers active on the current buffer")
    end
end

M.clangd_config = {
    cmd = {
        "clangd",
        "--all-scopes-completion",
        "--background-index",
        "--clang-tidy",
        "--completion-style=detailed",
        "--cross-file-rename",
        "--folding-ranges",
        "--header-insertion=iwyu",
        "--header-insertion-decorators",
    },
    init_options = {
        clangdFileStatus = true,
        usePlaceholders = true,
        completeUnimported = true,
    },
    commands = {
        ClangdSwitchSourceHeader = {
            function()
                switch_source_header(0, "e")
            end,
            description = "Switch between source/header",
        },
        ClangdSwitchSourceHeaderVs = {
            function()
                switch_source_header(0, "vs")
            end,
            description = "Switch between source/header in vertical split",
        },
        ClangdSwitchSourceHeaderSp = {
            function()
                switch_source_header(0, "sp")
            end,
            description = "Switch between source/header in horizontal split",
        },
    },
}

return M
