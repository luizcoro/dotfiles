require("mason-lspconfig").setup({
	ensure_installed = { "bashls", "clangd", "lua_ls", "basedpyright", "gopls" },
})

vim.diagnostic.config({ virtual_text = false, signs = false })
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.foldingRange = { dynamicRegistration = false, lineFoldingOnly = true }
capabilities.workspace.configuration = true
capabilities.textDocument.completion.completionItem.snippetSupport = true

local on_attach = function(client, buf)
	require("config.keymaps").on_lsp(client, buf)
	require("config.autocmds").on_lsp(client, buf)
end

local lsp = require("lspconfig")

local servers = {
	bashls = {},
	cmake = {},
	basedpyright = {},
	clangd = require("plugins.tools.lsp.cpp").clangd_config,
	r_language_server = {},
	zls = require("plugins.tools.lsp.zig").zls_config,
	lua_ls = require("plugins.tools.lsp.lua").lua_ls_config,
	texlab = {},
	ltex = require("plugins.tools.lsp.tex").ltex_config,
    gopls = {},
}

for server, config in pairs(servers) do
	config.capabilities = capabilities
	config.on_attach = on_attach
	lsp[server].setup(config)
end
