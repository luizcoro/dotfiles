local M = {}

M.lua_ls_config = {
    cmd = { "lua-language-server" },
    settings = {
        Lua = {
            hint = { enable = true },
            runtime = { version = "LuaJIT", path = vim.split(package.path, ";") },
            diagnostics = { enable = true, globals = { "vim" } },
            telemetry = { enable = false },
            workspace = {
                checkThirdParty = false,
                library = {
                    -- vim.env.VIMRUNTIME,
                    -- "${3rd}/luv/library",
                    -- "${3rd}/busted/library",
                    [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                    [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
                },
            },
        },
    },
}

-- workspace = {
--         checkThirdParty = false,
--         library = {
--           vim.env.VIMRUNTIME
--           -- Depending on the usage, you might want to add additional paths here.
--           -- "${3rd}/luv/library"
--           -- "${3rd}/busted/library",
--         }
--         -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
--         -- library = vim.api.nvim_get_runtime_file("", true)
--       }
--
return M
