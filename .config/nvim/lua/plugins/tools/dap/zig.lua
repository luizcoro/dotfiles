local M = {}

local get_program_path = function()
	return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
end

local get_program_args = function()
	local t = {}
	for i in string.gmatch(vim.fn.input("Args: "), "([^%s]+)") do
		t[#t + 1] = i
	end
	return t
end

function M.setup()
	local dap = require("dap")
	dap.adapters.lldb = {
		type = "executable",
		command = "/usr/bin/lldb-vscode",
		name = "lldb",
	}

	dap.adapters.codelldb = {
		type = "server",
		port = "${port}",
		executable = {
			command = "codelldb",
			args = { "--port", "${port}" },
		},
	}

	dap.configurations.zig = {
		{
			name = "Launch file with lldb-vscode",
			type = "lldb",
			request = "launch",
			program = get_program_path,
			cwd = "${workspaceFolder}",
			stopOnEntry = false,
			args = get_program_args,
			runInTerminal = true,
			env = function()
				local variables = {}
				for k, v in pairs(vim.fn.environ()) do
					table.insert(variables, string.format("%s=%s", k, v))
				end
				return variables
			end,
		},
		{
			name = "Launch file with codelldb",
			type = "codelldb",
			request = "launch",
			program = get_program_path,
			args = get_program_args,
			cwd = "${workspaceFolder}",
			stopOnEntry = false,
		},
	}
end

return M
