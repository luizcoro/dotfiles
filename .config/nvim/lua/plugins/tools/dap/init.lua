vim.fn.sign_define("DapBreakpoint", { text = "", texthl = "debugBreakpoint" })
vim.fn.sign_define("DapStopped", { text = "→", linehl = "debugPC", texthl = "debugPC" })

require("mason-nvim-dap").setup({
	ensure_installed = { "python", "codelldb", "bash", "delve" },
})

require("plugins.tools.dap.python").setup()
require("plugins.tools.dap.cpp").setup()
require("plugins.tools.dap.zig").setup()
require("plugins.tools.dap.go").setup()

local dap, dapui = require("dap"), require("dapui")

dapui.setup({
	layouts = {
		{
			elements = {
				{
					id = "scopes",
					size = 0.5, -- Can be float or integer > 1
				},
				{ id = "stacks", size = 0.25 },
				{ id = "watches", size = 0.25 },
			},
			size = 40,
			position = "left", -- Can be "left" or "right"
		},
		{
			elements = { "repl" },
			size = 10,
			position = "bottom", -- Can be "bottom" or "top"
		},
	},
})
dap.listeners.before.attach.dapui_config = function()
	dapui.open()
end
dap.listeners.before.launch.dapui_config = function()
	dapui.open()
end
dap.listeners.before.event_terminated.dapui_config = function()
	dapui.close()
end
dap.listeners.before.event_exited.dapui_config = function()
	dapui.close()
end

-- for _, item in pairs(require("config.keymaps").dap_keys()) do
--     vim.keymap.set(item.mode or {"n"}, item[1], item[2])
-- end
