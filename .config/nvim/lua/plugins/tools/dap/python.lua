local M = {}

local get_python_executable = function()
	local cwd = vim.fn.getcwd()
	if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
		return cwd .. "/venv/bin/python"
	elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
		return cwd .. "/.venv/bin/python"
	else
		return "/usr/bin/python"
	end
end

local get_program_args = function()
	local t = {}
	for i in string.gmatch(vim.fn.input("Args: "), "([^%s]+)") do
		t[#t + 1] = i
	end
	return t
end

function M.setup()
	local dap = require("dap")
	dap.adapters.python = function(cb, config)
		if config.request == "attach" then
			local port = (config.connect or config).port
			local host = (config.connect or config).host or "127.0.0.1"
			cb({
				type = "server",
				port = assert(port, "`connect.port` is required for a python `attach` configuration"),
				host = host,
				options = {
					source_filetype = "python",
				},
			})
		else
			cb({
				type = "executable",
				command = "debugpy-adapter",
				options = {
					source_filetype = "python",
				},
			})
		end
	end

	dap.adapters.poetry_python = function(cb, config)
		cb({
			type = "executable",
			command = "poetry",
			args = { "run", "python", "-m", "debugpy.adapter" },
			options = {
				source_filetype = "python",
			},
		})
	end

	dap.configurations.python = {
		{
			name = "Launch local file",
			type = "python",
			request = "launch",
			program = "${file}",
			cwd = "${workspaceFolder}",
			args = get_program_args,
			pythonPath = get_python_executable,
		},
		{
			name = "Launch local file (poetry)",
			type = "poetry_python",
			request = "launch",
			program = "${file}",
			cwd = "${workspaceFolder}",
			args = get_program_args,
			pythonPath = get_python_executable,
		},
		{
			type = "python",
			request = "attach",
			name = "Launch remote file",
			connect = {
				host = "127.0.0.1",
				port = 5678,
			},
			mode = "remote",
			cwd = "${workspaceFolder}",
			pathMappings = {
				{
					localRoot = "${workspaceFolder}",
					remoteRoot = "/",
				},
			},
		},
	}
end

return M
