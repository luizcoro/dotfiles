local M = {}

local get_program_path = function()
	return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
end

local get_program_args = function()
	local t = {}
	for i in string.gmatch(vim.fn.input("Args: "), "([^%s]+)") do
		t[#t + 1] = i
	end
	return t
end

function M.setup()
	local dap = require("dap")
	dap.adapters.delve = {
		type = "server",
		port = "${port}",
		executable = {
			command = "dlv",
			args = { "dap", "-l", "127.0.0.1:${port}" },
		},
	}

	-- https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv_dap.md
	dap.configurations.go = {
		{
			type = "delve",
			name = "Debug",
			request = "launch",
			program = "${file}",
		},
		{
			type = "delve",
			name = "Debug test", -- configuration for debugging test files
			request = "launch",
			mode = "test",
			program = "${file}",
		},
		-- works with go.mod packages and sub packages
		{
			type = "delve",
			name = "Debug test (go.mod)",
			request = "launch",
			mode = "test",
			program = "./${relativeFileDirname}",
		},
	}
end

return M
