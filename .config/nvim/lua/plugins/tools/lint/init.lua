require("lint").linters_by_ft = {
    -- lua = { "luacheck" },
    python = { "ruff" },
    json = { "jsonlint" },
    -- cpp = { "cppcheck" },
    -- tex = { "chktex" },
    sh = { "shellcheck" },
    -- make = { "checkmake" },
    all = { "cspell" },
}

require("mason-nvim-lint").setup({
    ensure_installed = {
        "ruff",
        "jsonlint",
        "shellcheck",
        -- "luacheck",
        "cspell",
    },
})
