require("conform").setup({
    formatters_by_ft = {
        lua = { "stylua" },
        python = { "isort", "black" },
        zig = { "zprint" },
        json = { "fixjson" },
        tex = { "latexindent" },
        cpp = { "clang-format" },
        c = { "clang-format" },
        sh = { "shfmt" },
    },
})

require("mason-conform").setup({
    ignore_install = { "zprint" },
})


-- for _, item in pairs(require("config.keymaps").conform_keys()) do
--     vim.keymap.set(item.mode or {"n"}, item[1], item[2])
-- end
