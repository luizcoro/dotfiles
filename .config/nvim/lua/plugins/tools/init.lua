require("mason").setup()

require("plugins.tools.lsp")
require("plugins.tools.format")
require("plugins.tools.lint")
require("plugins.tools.dap")
