require("nvim-treesitter.configs").setup({
	ensure_installed = {
		"bash",
		"bibtex",
		"c",
		"cpp",
		"latex",
		"lua",
		"python",
		"query",
		"vimdoc",
		"markdown",
		"cuda",
        "go",
        -- "zig",
	},
	highlight = { enable = true, additional_vim_regex_highlighting = false },
	indent = { enable = true },
	incremental_selection = {
		enable = true,
		keymaps = require("config.keymaps").ts_incremental_search_keys,
	},
	textobjects = {
		select = {
			enable = true,
			keymaps = require("config.keymaps").ts_textobjects_keys.select,
		},
		swap = {
			enable = true,
			swap_next = require("config.keymaps").ts_textobjects_keys.swap.next,
			swap_previous = require("config.keymaps").ts_textobjects_keys.swap.previous,
		},
		move = {
			enable = true,
			goto_next_start = require("config.keymaps").ts_textobjects_keys.move.next_start,
			goto_next_end = require("config.keymaps").ts_textobjects_keys.move.next_end,
			goto_previous_start = require("config.keymaps").ts_textobjects_keys.move.previous_start,
			goto_previous_end = require("config.keymaps").ts_textobjects_keys.move.previous_end,
		},
	},
	matchup = { enable = true },
	rainbow = { enable = true },
})

require("ibl").setup({
    indent = { char = "▏" },
	scope = { show_start = false, show_end = false },
})
