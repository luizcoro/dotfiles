require("toggleterm").setup()

for _, item in pairs(require("config.keymaps").toggleterm_keys()) do
    vim.keymap.set(item.mode or {"n"}, item[1], item[2])
end
