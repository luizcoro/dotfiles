require("oil").setup({
	columns = { "icon", "permissions", "size", "mtime" },
    default_file_explorer = true,
	delete_to_trash = true,
	skip_confirm_for_simple_edits = true,
	view_options = { show_hidden = true },
})

-- for _, item in pairs(require("config.keymaps").oil_keys()) do
-- 	vim.keymap.set(item.mode or { "n" }, item[1], item[2])
-- end
