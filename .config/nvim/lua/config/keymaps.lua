local M = {}

-- function ToggleQuickfixWindow()
-- 	if vim.fn.getqflist({ winid = 0 }).winid > 0 then
-- 		vim.cmd("cclose")
-- 	else
-- 		vim.cmd("copen")
-- 	end
-- end
--
-- function ToggleLoclistWindow()
-- 	if vim.fn.getloclist(0, { winid = 0 }).winid > 0 then
-- 		vim.cmd("lclose")
-- 	else
-- 		vim.cmd("lopen")
-- 	end
-- end
--
-- function ToggleDiagWindow()
-- 	if vim.fn.getloclist(0, { winid = 0 }).winid > 0 then
-- 		vim.cmd("lclose")
-- 	else
-- 		vim.diagnostic.setloclist()
-- 	end
-- end

M.on_verylazy = function()
	vim.keymap.set({ "n" }, "<C-S>", ":update | normal zx<CR>")
	vim.keymap.set({ "i", "v" }, "<C-S>", "<C-C>:update | normal zx<CR>")
	vim.keymap.set({ "n" }, "]a", ":next<CR>")
	vim.keymap.set({ "n" }, "[a", ":previous<CR>")
	vim.keymap.set({ "n" }, "]A", ":last<CR>")
	vim.keymap.set({ "n" }, "[A", ":first<CR>")
	vim.keymap.set({ "n" }, "]b", ":bnext<CR>")
	vim.keymap.set({ "n" }, "[b", ":bprevious<CR>")
	vim.keymap.set({ "n" }, "]B", ":blast<CR>")
	vim.keymap.set({ "n" }, "[B", ":bfirst<CR>")
	vim.keymap.set({ "n" }, "]t", ":tabnext<CR>")
	vim.keymap.set({ "n" }, "[t", ":tabprevious<CR>")
	vim.keymap.set({ "n" }, "]T", ":tablast<CR>")
	vim.keymap.set({ "n" }, "[T", ":tabfirst<CR>")
	vim.keymap.set({ "n" }, "]q", ":cnext<CR>zz")
	vim.keymap.set({ "n" }, "[q", ":cprevious<CR>zz")
	vim.keymap.set({ "n" }, "]Q", ":clast<CR>zz")
	vim.keymap.set({ "n" }, "[Q", ":cfirst<CR>zz")
	vim.keymap.set({ "n" }, "]l", ":lnext<CR>zz")
	vim.keymap.set({ "n" }, "[l", ":lprevious<CR>zz")
	vim.keymap.set({ "n" }, "]L", ":llast<CR>zz")
	vim.keymap.set({ "n" }, "[L", ":lfirst<CR>zz")
	vim.keymap.set({ "v" }, "<LeftRelease>", '"*ygv')
	vim.keymap.set({ "t" }, "<Esc>", "<C-\\><C-n>")
	vim.keymap.set({ "t" }, "<A-]>", "<Esc>")
	vim.keymap.set({ "v" }, "<", "<gv")
	vim.keymap.set({ "v" }, ">", ">gv")
	vim.keymap.set({ "n" }, "<Leader>n", ":noh<cr>")
	-- vim.keymap.set({ "n", "i" }, "<M-f>", ":20Lexplore<CR>")
	-- vim.keymap.set({"n"}, "<Leader>tq", ToggleQuickfixWindow)
	-- vim.keymap.set({"n"}, "<Leader>tl", ToggleLoclistWindow)
	-- vim.keymap.set({"n"}, "<Leader>td", ToggleDiagWindow)
end

M.on_lsp = function(client, buf)
	local opts = { buffer = buf }
	vim.keymap.set({ "n" }, "[d", ':lua vim.diagnostic.goto_prev({float = {border = "rounded"}})<CR>zz', opts)
	vim.keymap.set({ "n" }, "]d", ':lua vim.diagnostic.goto_next({float = {border = "rounded"}})<CR>zz', opts)
	-- vim.keymap.set({ "n" }, "K", vim.lsp.buf.hover, opts)
	-- vim.keymap.set({ "i" }, "<C-k>", vim.lsp.buf.signature_help, opts)
	vim.keymap.set({ "n" }, "<Leader>gd", vim.lsp.buf.definition, opts)
	vim.keymap.set({ "n" }, "<Leader>gD", vim.lsp.buf.declaration, opts)
	vim.keymap.set({ "n" }, "<Leader>gt", vim.lsp.buf.type_definition, opts)
	vim.keymap.set({ "n" }, "<Leader>lr", vim.lsp.buf.rename, opts)
	vim.keymap.set({ "n" }, "<Leader>la", vim.lsp.buf.code_action, opts)
	-- vim.keymap.set({ "n" }, "<Leader>os", vim.lsp.buf.document_symbol, opts)
	-- vim.keymap.set({ "n" }, "<Leader>oS", vim.lsp.buf.workspace_symbol, opts)
	-- vim.keymap.set({ "n" }, "<Leader>oi", vim.lsp.buf.implementation, opts)
	-- vim.keymap.set({ "n" }, "<Leader>or", vim.lsp.buf.references, opts)
	vim.keymap.set({ "n" }, "<Leader>wa", vim.lsp.buf.add_workspace_folder, opts)
	vim.keymap.set({ "n" }, "<Leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
	vim.keymap.set({ "n" }, "<Leader>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, opts)

	vim.keymap.set({ "n" }, "<Leader>th", function()
		vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled({ bufnr = buf }), { bufnr = buf })
	end)

	if client.name == "clangd" then
		vim.keymap.set({ "n" }, "<Leader>ae", ":ClangdSwitchSourceHeader<CR>", opts)
		vim.keymap.set({ "n" }, "<Leader>av", ":ClangdSwitchSourceHeaderVs<CR>", opts)
		vim.keymap.set({ "n" }, "<Leader>ax", ":ClangdSwitchSourceHeaderSp<CR>", opts)
	end

	if client.name == "texlab_lsp" then
		vim.keymap.set({ "n" }, "<Leader>lv", ":TexlabForward<CR>", opts)
	end
end

-- stylua: ignore start

M.telescope_keys = function()
	return {
		{ "<Leader>of", function() require("telescope.builtin").find_files({ previewer = false }) end },
		{ "<Leader>oc", function() require("telescope.builtin").find_files({ previewer = false, cwd = vim.fn.stdpath("config") }) end },
		{ "<Leader>gg", function() require("telescope.builtin").live_grep() end },
		{ "<Leader>ol", function() require("telescope.builtin").loclist() end },
		{ "<Leader>om", function() require("telescope.builtin").man_pages() end },
		{ "<Leader>o'", function() require("telescope.builtin").marks() end },
		{ "<Leader>oq", function() require("telescope.builtin").quickfix() end },
		{ "<Leader>os", function() require("telescope.builtin").dynamic_workspace_symbols() end },
		{ "<Leader>or", function() require("telescope.builtin").lsp_references() end },
		{ "<Leader>od", function() require("telescope.builtin").lsp_definitions() end },
		{ "<Leader>oi", function() require("telescope.builtin").lsp_implementations() end },
	}
end


M.dap_keys = function()
    return {
        { "<Leader>dt", function() require("dapui").toggle() end },
        { "<Leader>de", function() require("dapui").eval() end },
        { "<Leader>dl", function() require("dap").run_last() end },
        { "<Leader>dc", function() require("dap").continue() end },
        { "<Leader>dn", function() require("dap").step_over() end },
        { "<Leader>di", function() require("dap").step_into() end },
        { "<Leader>do", function() require("dap").step_out() end },
        { "<Leader>db", function() require("dap").step_back() end },
        { "<Leader>du", function() require("dap").up() end },
        { "<Leader>dd", function() require("dap").down() end },
        { "<Leader>db", function() require("dap").toggle_breakpoint() end },
        { "<Leader>dB", function() require("dap").set_breakpoint(vim.fn.input("Breakpoint Condition: ")) end },
    }
end


M.transfer_keys = function()
    return {
        { "<Leader>ui", "<cmd>TransferInit<cr>", mode = "n" },
        { "<Leader>ud", "<cmd>TransferDownload<cr>", mode = "n" },
        { "<Leader>uu", "<cmd>TransferUpload<cr>", mode = "n" },
        { "<Leader>uf", "<cmd>DiffRemote<cr>", mode = "n" },
        { "<Leader>uF", "<cmd>TransferDirDiff<cr>", mode = "n" },
        { "<Leader>ur", "<cmd>TransferRepeat<cr>", mode = "n" },
    }
end

M.conform_keys = function()
    return {
        { "<Leader>f", function() require("conform").format({
            lsp_fallback = true,
            async = false,
            timeout_ms = 500,
        }) end, mode = {"n", "v"} },
    }
end

M.oil_keys = function()
    return {
        {"-", "<cmd>Oil<cr>", mode = "n"}
    }
end

M.snippy_keys = function()
    return {
        { "<C-x><C-s>", function() require("snippy").complete() end, mode = "i" },
        { "<Tab>", function() require("snippy.mapping").expand_or_advance("<Tab>")() end, mode = { "i", "s" } },
        { "<S-Tab>", function() require("snippy.mapping").previous("<S-Tab>")() end, mode = { "i", "s" } },
    }
end

M.luasnip_keys = function()
    return {
        { "<C-k>", function() require("luasnip").jump(1) end, mode = { "i", "s" } },
        { "<C-j>", function() require("luasnip").jump(-1) end, mode = { "i", "s" } },
    }
end


M.toggleterm_keys = function()
    return {
        { "<M-t>", ":ToggleTerm<CR>" },
        { "<M-t>", "<C-\\><C-n>:ToggleTerm<CR>", mode = "t" },
        { "<Leader>ss", function() require("toggleterm").send_lines_to_terminal("single_line", false, { args = vim.v.count }) end },
        { "<Leader>s", function() require("toggleterm").send_lines_to_terminal("visual_lines", false, { args = vim.v.count }) end, mode = "x" },
    }
end

M.ufo_keys = function()
    return {
        { "zR", require("ufo").openAllFolds },
        { "zM", require("ufo").closeAllFolds },
        { "zr", require("ufo").openFoldsExceptKinds },
        { "zm", require("ufo").closeFoldsWith },
        { "K", function()
            if not require("ufo").peekFoldedLinesUnderCursor() then
                vim.lsp.buf.hover()
            end
        end},
    }
end

M.hlslens_keys = function()
    local kopts = {noremap = true, silent = true}
    return {
        { 'n', [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts },
        { 'N', [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts },
        { '*', [[*<Cmd>lua require('hlslens').start()<CR>]], kopts},
        { '#', [[#<Cmd>lua require('hlslens').start()<CR>]], kopts},
        { 'g*', [[g*<Cmd>lua require('hlslens').start()<CR>]], kopts},
        { 'g#', [[g#<Cmd>lua require('hlslens').start()<CR>]], kopts},
        { '<Leader>n', '<Cmd>noh<CR>', kopts},
    }
end

M.gitsigns_keys = function()
	return {
		{ "]c",
			function()
				if vim.wo.diff then
					vim.cmd.normal({ "]c", bang = true })
				else
					require("gitsigns").nav_hunk("next")
				end
			end,
		},
		{ "[c",
			function()
				if vim.wo.diff then
					vim.cmd.normal({ "[c", bang = true })
				else
					require("gitsigns").nav_hunk("prev")
				end
			end,
		},
		{ "<leader>hs", function() require("gitsigns").stage_hunk() end },
		{ "<leader>hr", function() require("gitsigns").reset_hunk() end },
		{ "<leader>hs", function() require("gitsigns").stage_hunk({ vim.fn.line("."), vim.fn.line("v") }) end, mode = "v" },
		{ "<leader>hr", function() require("gitsigns").reset_hunk({ vim.fn.line("."), vim.fn.line("v") }) end, mode = "v" },
		{ "<leader>hS", function() require("gitsigns").stage_buffer() end },
		{ "<leader>hu", function() require("gitsigns").undo_stage_hunk() end },
		{ "<leader>hR", function() require("gitsigns").reset_buffer() end },
		{ "<leader>hp", function() require("gitsigns").preview_hunk() end },
		{ "<leader>hb", function() require("gitsigns").blame_line({ full = true }) end },
		{ "<leader>tb", function() require("gitsigns").toggle_current_line_blame() end },
		{ "<leader>td", function() require("gitsigns").toggle_deleted() end },
		{ "ih", ":<C-U>Gitsigns select_hunk<CR>", mode = {"x", "o"} },
	}
end


M.diffview_keys = function()
    return {}
end

M.chat_keys = function()
	return {
		{ "<A-c>", "<cmd>PrtChatToggle popup<cr>", mode = { "n", "i" } },
		{ "<Leader>cn", "<cmd>PrtChatNew popup<cr>", mode = { "n" } },
		{ "<Leader>cn", ":<C-u>'<,'>PrtChatNew<cr>", mode = { "v" } },

		{ "<Leader>cx", "<cmd>PrtContext<cr>", mode = { "n" } },
		{ "<Leader>cv", "<cmd>PrtProvider<cr>", mode = { "n" } },
		{ "<Leader>cg", "<cmd>PrtModel<cr>", mode = { "n" } },
		{ "<Leader>ch", "<cmd>PrtChatFinder<cr>", mode = { "n" } },
		{ "<Leader>cs", "<cmd>PrtStop<cr>", mode = { "n", "v", "x" } },

		{ "<Leader>cr", "<cmd>PrtRewrite<cr>", mode = { "n" } },
		{ "<Leader>cr", ":<C-u>'<,'>PrtRewrite<cr>", mode = { "v" } },
		{ "<Leader>ci", ":<C-u>'<,'>PrtImplement<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>cc", ":<C-u>'<,'>PrtComplete<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>ce", ":<C-u>'<,'>PrtExplain<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>cf", ":<C-u>'<,'>PrtFixBugs<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>co", ":<C-u>'<,'>PrtOptimize<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>cu", ":<C-u>'<,'>PrtUnitTests<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>cd", ":<C-u>'<,'>PrtDebug<cr>", mode = { "n", "v", "x" } },
		{ "<Leader>cp", "<cmd>PrtCommitMsg<cr>", mode = { "n"} },
		{ "<Leader>cp", "<cmd>PrtProofReader<cr>", mode = { "n"} },
	}
end


M.ts_incremental_search_keys = {
    init_selection = "<CR>",
    scope_incremental = "<CR>",
    node_incremental = "<TAB>",
    node_decremental = "<S-TAB>",
}

M.ts_textobjects_keys = {
    select = {
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ac"] = "@class.outer",
        ["ic"] = "@class.inner",
        ["a,"] = "@parameter.outer",
        ["i,"] = "@parameter.inner",
    },
    swap = {
        next = { ["<M-l>"] = "@parameter.inner" },
        previous = { ["<M-h>"] = "@parameter.inner" },
    },
    move = {
        next_start = { ["]f"] = "@function.outer", ["]s"] = "@class.outer" },
        next_end = { ["]F"] = "@function.outer", ["]s"] = "@class.outer" },
        previous_start = { ["[f"] = "@function.outer", ["[s"] = "@class.outer" },
        previous_end = { ["[F"] = "@function.outer", ["[s"] = "@class.outer" },
    }
}

-- stylua: ignore end

return M
