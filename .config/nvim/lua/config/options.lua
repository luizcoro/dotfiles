vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.g.matchup_matchparen_offscreen = { method = "" }
vim.g.zig_fmt_autosave = false
-- vim.g.rainbow_active = true
vim.opt.termguicolors = true
vim.opt.shiftround = true
vim.opt.showmode = false
vim.opt.pumheight = 10
vim.opt.updatetime = 300
vim.opt.lazyredraw = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.directory = vim.fn.stdpath("state") .. "/swap"
vim.opt.backupdir = vim.fn.stdpath("state") .. "/backup"
vim.opt.undodir = vim.fn.stdpath("state") .. "/undo"
vim.opt.backup = true
vim.opt.hidden = true
vim.opt.completeopt = { "menu", "longest" }
vim.opt.shortmess = vim.opt.shortmess + "Ssc"
vim.opt.inccommand = "nosplit"
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.undofile = true
vim.opt.spelllang = "en_us,fr_fr,pt_br"
vim.opt.scrolloff = 5
vim.opt.sidescrolloff = 5
vim.opt.cursorline = true
vim.opt.foldlevel = 99
vim.opt.foldlevelstart = 99
vim.opt.foldenable = true
vim.opt.splitkeep = "screen"
vim.opt.statusline = "%!v:lua.MyStatusline()"
-- vim.opt.tabline = "%!v:lua.MyTabline()"

function MyStatusline()
    return "%#StatusLineTextBold# %n "
        .. "%#StatusLineText# %<%f%( %m%r%) "
        .. "%#StatusLine# %= "
        .. "%#StatusLineText# %{get(b:,'gitsigns_status','')} "
        .. "%#StatusLineText# %l:%c "
        .. "%#StatusLinePerc# %P "
end

-- function MyTabline()
--     local s = "%="
--     local tabs = vim.api.nvim_list_tabpages()
--     local current_tab = vim.api.nvim_get_current_tabpage()
--     for _, tabid in ipairs(tabs) do
--         s = s .. "%#TabLineFill#%T"
--         if tabid == current_tab then
--             s = s .. "%#TabLineSel#"
--         else
--             s = s .. "%#TabLine#"
--         end
--         local winnr = vim.api.nvim_tabpage_get_win(tabid)
--         local dir = vim.fn.getcwd(winnr, tabid):match("^.+/(.+)$")
--         s = s .. "%" .. tabid .. "T " .. tabid .. " " .. dir .. " "
--     end
--     s = s .. "%#TabLineFill#%T"
--     return s
-- end
