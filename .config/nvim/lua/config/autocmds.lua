local M = {}

M.on_startup = function()
	vim.api.nvim_create_autocmd("BufWritePre", {
		pattern = "*",
		desc = "remove trailing whitespaces",
		callback = function()
			local curpos = vim.api.nvim_win_get_cursor(0)
			vim.cmd([[keeppatterns %s/\s\+$//e]])
			vim.api.nvim_win_set_cursor(0, curpos)
		end,
	})

	vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
		pattern = "*",
		desc = "return cursor to where it was last time closing the file",
		command = 'silent! normal! g`"zv',
	})
end

M.on_verylazy = function()
	vim.api.nvim_create_autocmd({ "TextYankPost" }, {
		pattern = "*",
		desc = "highlight yanked text",
		callback = function()
			require("vim.highlight").on_yank()
		end,
	})

	vim.api.nvim_create_autocmd({ "BufWritePost" }, {
		pattern = "*",
		desc = "apply linters",
		callback = function()
			require("lint").try_lint()
			require("lint").try_lint("codespell")
		end,
	})

	-- vim.api.nvim_create_autocmd("CompleteDone", {
	-- 	pattern = "*",
	-- 	desc = "trigger snippet completion",
	-- 	callback = function()
	-- 		require("snippy").complete_done()
	-- 	end,
	-- })
end

M.on_lsp = function(client, buf)
	if client.server_capabilities.inlayHintProvider then
		vim.lsp.inlay_hint.enable(false, { bufnr = buf })
	end

	if client.server_capabilities.documentHighlightProvider then
		vim.api.nvim_create_augroup("lsp_document_highlight", {
			clear = false,
		})
		vim.api.nvim_clear_autocmds({
			buffer = buf,
			group = "lsp_document_highlight",
		})
		vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
			group = "lsp_document_highlight",
			buffer = buf,
			callback = vim.lsp.buf.document_highlight,
		})
		vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
			group = "lsp_document_highlight",
			buffer = buf,
			callback = vim.lsp.buf.clear_references,
		})
	end
end

return M
