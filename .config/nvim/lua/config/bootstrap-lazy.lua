local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
		"zbirenbaum/copilot.lua",
		cmd = "Copilot",
		event = "InsertEnter",
		config = function()
			require("copilot").setup({
				suggestion = { enabled = false },
				panel = { enabled = false },
			})
		end,
	},
	{
		"coffebar/transfer.nvim",
		cmd = {
			"TransferInit",
			"DiffRemote",
			"TransferUpload",
			"TransferDownload",
			"TransferDirDiff",
			"TransferRepeat",
		},
		keys = require("config.keymaps").transfer_keys,
		opts = {},
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lua",
			"hrsh7th/cmp-path",
			"dcampos/cmp-snippy",
			"saadparwaiz1/cmp_luasnip",
			"onsails/lspkind.nvim",
			{
				"zbirenbaum/copilot-cmp",
				config = function()
					require("copilot_cmp").setup()
				end,
			},
			{

				"L3MON4D3/LuaSnip",
				tag = "v2.3.0",
				run = "make install_jsregexp",
				keys = require("config.keymaps").luasnip_keys,
			},
		},
		lazy = false,
		config = function()
			require("plugins.completion")
		end,
	},
	{ "williamboman/mason.nvim", lazy = false, opts = true },
	{
		"stevearc/oil.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		lazy = false,
		keys = require("config.keymaps").oil_keys,
		config = function()
			require("plugins.oil")
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
			"andymass/vim-matchup",
			"HiPhish/rainbow-delimiters.nvim",
			{ "lukas-reineke/indent-blankline.nvim", main = "ibl" },
		},
		build = ":TSUpdate",
		event = "BufReadPost",
		config = function()
			require("plugins.highlight")
		end,
	},
	{

		"neovim/nvim-lspconfig",
		dependencies = {
			"williamboman/mason-lspconfig.nvim",
			{
				"SmiteshP/nvim-navbuddy",
				dependencies = {
					"SmiteshP/nvim-navic",
					"MunifTanjim/nui.nvim",
				},
				opts = { lsp = { auto_attach = true } },
			},
		},
		event = { "BufReadPre", "BufNewFile" },
		config = function()
			require("plugins.tools.lsp")
		end,
	},
	{
		"mfussenegger/nvim-lint",
		dependencies = { "rshkarin/mason-nvim-lint" },
		event = { "BufReadPre", "BufNewFile" },
		config = function()
			require("plugins.tools.lint")
		end,
	},
	{
		"stevearc/conform.nvim",
		dependencies = { "zapling/mason-conform.nvim" },
		keys = require("config.keymaps").conform_keys,
		config = function()
			require("plugins.tools.format")
		end,
	},
	{
		"rcarriga/nvim-dap-ui",
		dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio", "jay-babu/mason-nvim-dap.nvim" },
		keys = require("config.keymaps").dap_keys,
		config = function()
			require("plugins.tools.dap")
		end,
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		keys = require("config.keymaps").telescope_keys,
		config = function()
			require("plugins.telescope")
		end,
	},
	{
		"kevinhwang91/nvim-ufo",
		dependencies = { "kevinhwang91/promise-async" },
		keys = require("config.keymaps").ufo_keys,
		config = function()
			require("plugins.ufo")
		end,
	},
	{
		"frankroeder/parrot.nvim",
		dependencies = { "ibhagwan/fzf-lua", "nvim-lua/plenary.nvim" },
		keys = require("config.keymaps").chat_keys,
		config = function()
			require("plugins.chat")
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		event = { "BufReadPre", "BufNewFile" },
		opts = {
			on_attach = function(bufnr)
				for _, item in pairs(require("config.keymaps").gitsigns_keys()) do
					vim.keymap.set(item.mode or { "n" }, item[1], item[2], { buffer = bufnr })
				end
			end,
		},
	},
	{ "sindrets/diffview.nvim", event = { "BufReadPre", "BufNewFile" }, opts = {} },
	{ "numToStr/Comment.nvim", event = "BufReadPre", opts = {} },
	{ "akinsho/toggleterm.nvim", keys = require("config.keymaps").toggleterm_keys, opts = {} },
}, {
	defaults = { lazy = true },
	performance = {
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"netrwPlugin",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})
